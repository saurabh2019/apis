//require('dotenv/config');
const config = require('config');
const http = require('http');
const app = require('./app');
const server = http.createServer(app);
server.listen(config.port, '0.0.0.0');
console.log('Magic happens at http://localhost:' + config.port);