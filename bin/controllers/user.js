
const errors = require('../custom/errors')
const userSchema = require('../models/user');
const async = require('async')

module.exports = {

    inserUserData: async (req, res) => {
        try {
            const usersData = [{
                userId: 1,
                name: 'Rahul'
            },
            {
                userId: 2,
                name: 'Ramesh'
            },
            {
                userId: 3,
                name: 'Ankita'
            }];
            let usersCreated = await userSchema.create(usersData);

            if (!usersCreated) throw errors.userCreation;

            return res.status(200).json({
                error: false,
                user: usersCreated,
                message: 'data saved successfully',
                success: true
            });
        }

        catch (err) {
            return res.status(err.code).json({
                error: true,
                details: err
            });
        }
    },
    calculateOrderBill: async (req, res) => {
        try {
            const condition = [
                {
                    $lookup: {
                        from: 'orders',
                        localField: 'userId',
                        foreignField: 'userId',
                        as: 'orders'
                    }
                },
                {
                    $addFields: {
                        noOfOrders: {
                            $size: '$orders'
                        },
                        averageBillValue: {
                            $floor: {
                                $divide: [
                                    { $sum: '$orders.subtotal' }, { $size: '$orders' }
                                ],
                            }
                        }
                    }
                },
                {
                    $project: {
                        orders: 0,
                        _id: 0
                    }
                }

            ];
            let calculateData = await userSchema.aggregate(condition);
            if (!calculateData) throw errors.errorGetUserData;
            else if (calculateData.length == 0) {
                return res.status(201).json({ success: false, message: 'no data found' })
            }
            else {
                return res.status(200).json({ success: true, user: calculateData })
            }


        }
        catch (err) {
            return res.status(err.code).json({
                error: true,
                details: err
            });
        }

    },
    updateUsers: async (req, res) => {
        try {
            const condition = [
                {
                    $lookup: {
                        from: 'orders',
                        localField: 'userId',
                        foreignField: 'userId',
                        as: 'orders'
                    }
                },
                {
                    $addFields: {
                        noOfOrders: {
                            $size: '$orders'
                        },

                    }
                },

            ]
            const findUserOrders = await userSchema.aggregate(condition);
            if (!findUserOrders) throw errors.errorGetUserOrders;
            else if (findUserOrders.length == 0) {
                return res.status(201).json({ success: false, message: 'no data found' })
            }
            else {
                async.each(findUserOrders, (user, callback) => {
                    let condition = {
                        userId: user.userId
                    };
                    let update = {
                        $set: {
                            noOfOrders: user.noOfOrders
                        }
                    };
                    let Option = {};
                    userSchema.updateOne(condition, update, Option, (err, updated) => {
                        if (err) {
                            callback({ error: true, success: false });

                        }
                        else {
                            callback();
                        }
                    })
                }, (err) => {
                    if (err) throw errors.errorUpdateUser;
                    else return res.status(200).json({ success: true, message: "Successfully updated" })
                });



            }

        }
        catch (err) {
            return res.status(err.code).json({
                error: true,
                details: err
            });
        }

    }
}


