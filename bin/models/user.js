const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userSchema = new Schema({
    userId: Number,
    name: String,
    noOfOrders: { type: Number, default: 0 }

});


module.exports = mongoose.model('user', userSchema);
