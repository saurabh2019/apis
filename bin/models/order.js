const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let orderSchema = new Schema({
    userId: Number,
    orderId: Number,
    subtotal: Number,
    date: String

});


module.exports = mongoose.model('order', orderSchema);
