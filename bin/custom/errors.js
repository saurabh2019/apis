const CustomError = require('./error');



const userCreation = new CustomError('error occured', 'something went wrong in user creation', 400);

const orderCreation = new CustomError('error occured', 'something went wrong in order creation', 400);

const errorGetUserData = new CustomError('error occured', 'something went wrong in get order number and bills', 400);

const errorGetUserOrders = new CustomError('error occured', 'something went wrong in get user orders', 400);

const errorUpdateUser = new CustomError('error occured', 'something went wrong in errorUpdateUser', 400);




module.exports = {
    userCreation,
    orderCreation,
    errorGetUserData, errorGetUserOrders,errorUpdateUser
};