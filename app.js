'use strict';
/**
 * Module dependencies.
 */

const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    morgan = require('morgan'),
    CustomError = require('./bin/custom/error'),
    config = require('config'); // get our config file




//******* IMPORTING ROUTES *******\\
const routes = require('./bin/routes/route');



// =======================
// configuration =========
// =======================
mongoose.connect(config.database, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}); // connect to database
console.log("MongoDb Connection: ", config.database);


// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + config.database);
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection 


mongoose.set('debug', true);

//******* SETTING CORS HEADER *******\\
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
        return res.status(200).json({});
    }
    next();
});
//******* HIDING EXPRESS *******\\
app.set('x-powered-by', false);
app.use(function (req, res, next) {
    res.header("Efforts", "Team Daxy with Blood, sweat and tears. if you see this i consider you smart!");
    next();
});

//******* MIDDLEWARES *******\\
app.use(morgan('dev'));
app.use(express.json());
app.use(require('body-parser').json());
app.use(require('body-parser').urlencoded({
    extended: true
}));

//******* USING THE IMPORTED ROUTES *******\\
app.use('/api', routes);




//******* ERROR HANDLING *******\\
app.use((req, res, next) => {
    const error = new CustomError('Not Found!', `Uh oh! the path you are trying to reach we can't find it, we've checked each an every corner!`, 404);
    next(error);

});

app.use((error, req, res, next) => {

    res.status(error.code || 500).json({
        error: true,
        details: error
    });
});

module.exports = app;





















